/**
 * We'll create a new custom component and register it with Handlebars to make it available to our templates.
 **/
Todos.EditTodoView = Ember.TextField.extend({
  didInsertElement: function () {
    this.$().focus();
  }
});

Ember.Handlebars.helper('edit-todo', Todos.EditTodoView);