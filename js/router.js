/**
 * This will tell Ember.js to detect when the application's URL matches '/' and to render the todos template.
 **/
Todos.Router.map(function () {
  this.resource('todos', { path: '/' }, function () {
  	/**
  	 * Update the router to change the todos mapping so it can accept child routes
  	 **/
  	// additional child routes
  	this.route('active');
    this.route('completed');
  });
});

/**
 * Because we hadn't implemented this class before, Ember.js provided a Route for us with the default behavior
 * of rendering a matching template named todos using its naming conventions for object creation.
 **/
Todos.TodosRoute = Ember.Route.extend({
  model: function () {
    return this.store.find('todo');
  }
});

/**
 * When the application loads at the url '/' Ember.js will enter the todos route and render the todos template as before. It will
 * also transition into the todos.index route and fill the {{outlet}} in the todos template with the todos/index template. The model
 * data for this template is the result of the model method of TodosIndexRoute, which indicates that the model for this route is the
 * same model for the TodosRoute.
 **/
Todos.TodosIndexRoute = Ember.Route.extend({
  model: function () {
    return this.modelFor('todos');
  }
});

/**
 * The model data for this route is the collection of todos whose isCompleted property is false. When a todo's isCompleted property
 * changes this collection will automatically update to add or remove the todo appropriately.
 *
 * Normally transitioning into a new route changes the template rendered into the parent {{outlet}}, but in this case we'd like to
 * reuse the existing todos/index template. We can accomplish this by implementing the renderTemplate method and calling render
 * ourselves with the specific template and controller options.
 **/
Todos.TodosActiveRoute = Ember.Route.extend({
  model: function(){
    return this.store.filter('todo', function (todo) {
      return !todo.get('isCompleted');
    });
  },
  renderTemplate: function(controller){
    this.render('todos/index', {controller: controller});
  }
});

/**
 * The model data for this route is the collection of todos whose isCompleted property is true. When a todo's isCompleted property
 * changes this collection will automatically update to add or remove the todo appropriately.
 *
 * Normally transitioning into a new route changes the template rendered into the parent {{outlet}}, but in this case we'd like to
 * reuse the existing todos/index template. We can accomplish this by implementing the renderTemplate method and calling render ourselves
 * with the specific template and controller options.
 **/
Todos.TodosCompletedRoute = Ember.Route.extend({
  model: function(){
    return this.store.filter('todo', function (todo) {
      return todo.get('isCompleted');
    });
  },
  renderTemplate: function(controller){
    this.render('todos/index', {controller: controller});
  }
});