/**
 * Because we have not needed a custom controller behavior until this point, Ember.js provided a default controller
 * object for this template. To handle our new behavior, we can implement the controller class Ember.js expects to find
 * according to its naming conventions and add our custom behavior. This new controller class will automatically be associated
 * with this template for us.
 **/
Todos.TodosController = Ember.ArrayController.extend({
  // the initial value of the 'newTitle' property
  newTitle: '',

  /**
   * The remaining property will return the number of todos whose isCompleted property is false. If the isCompleted value of any
   * todo changes, this property will be recomputed. If the value has changed, the section of the template displaying the count will
   * be automatically updated to reflect the new value.
   **/
  remaining: function () {
    return this.filterBy('isCompleted', false).get('length');
  }.property('@each.isCompleted'),

  /**
   * The inflection property will return either a plural or singular version of the word "item" depending on how many todos are
   * currently in the list. The section of the template displaying the count will be automatically updated to reflect the new value.
   **/
  inflection: function () {
    var remaining = this.get('remaining');
    return remaining === 1 ? 'item' : 'items';
  }.property('remaining'),

  actions: {
    /**
     * This controller will now respond to user action by using its newTitle property as the title of a new todo whose isCompleted
     * property is false. Then it will clear its newTitle property which will synchronize to the template and reset the textfield. Finally,
     * it persists any unsaved changes on the todo.
     **/
    createTodo: function () {
      // Get the todo title set by the "New Todo" text field
      var title = this.get('newTitle');
      if (!title.trim()) { return; }

      // Create the new Todo model
      var todo = this.store.createRecord('todo', {
        title: title,
        isCompleted: false
      });

      // Clear the "New Todo" text field
      this.set('newTitle', '');

      // Save the new model
      todo.save();
    },

    /**
     * See 001)
     **/
    clearCompleted: function () {
      var completed = this.filterBy('isCompleted', true);
      completed.invoke('deleteRecord');
      completed.invoke('save');
    }
  },

  /**
   * 001)
   * The completed and clearCompleted methods both invoke the filterBy method, which is part of the ArrayController API and returns an
   * instance of EmberArray which contains only the items for which the callback returns true. The clearCompleted method also invokes
   * the invoke method which is part of the EmberArray API. invoke will execute a method on each object in the Array if the method exists
   * on that object.
   **/
  hasCompleted: function () {
    return this.get('completed') > 0;
  }.property('completed'),

  completed: function () {
    return this.filterBy('isCompleted', true).get('length');
  }.property('@each.isCompleted'),

  /**
   * To implement this behavior update the allAreDone property to handle both getting and setting behavior.
   * If no value argument is passed this property is being used to populate the current value of the checkbox. If a value is passed it
   * indicates the checkbox was used by a user and we should set the isCompleted property of each todo to this new value.
   **/
  allAreDone: function (key, value) {
    if (value === undefined) {
      /**
       * This property will be true if the controller has any todos and every todo's isCompleted property is true. If the isCompleted property
       * of any todo changes, this property will be recomputed. If the return value has changed, sections of the template that need to update
       * will be automatically updated for us.
       **/
      return !!this.get('length') && this.everyBy('isCompleted', true);
    } else {
      this.setEach('isCompleted', value);
      this.invoke('save');
      return value;
    }
  }.property('@each.isCompleted')

});