/**
 * When called from the template to display the current isCompleted state of the todo, this property will proxy that
 * question to its underlying model. When called with a value because a user has toggled the checkbox in the template,
 * this property will set the isCompleted property of its model to the passed value (true or false), persist the model
 * update, and return the passed value so the checkbox will display correctly. 
 **/
Todos.TodoController = Ember.ObjectController.extend({
  /**
   * http://stackoverflow.com/questions/12596619/objectcontroller-and-arraycontroller
   *
   * Usually, if your Controller represent a list of items, you would use the Ember.ArrayController, and if the controller represents
   * a single item, you would use the Ember.ObjectController. Something like the following:
   *
   * MyApp.ContactsController = Ember.ArrayController.extend({
   *    content: [],
   *    selectedContact: null
   * });
   *
   * MyApp.SelectedContactController = Ember.ObjectController.extend({
   *   contentBinding: 'contactsController.selectedContact',
   *   contactsController: null
   * });
   **/

  /**
   * We defined an initial isEditing value of false for controllers of this type and said that when the editTodo action is
   * called it should set the isEditing property of this controller to true. This will automatically trigger the sections of
   * template that use isEditing to update their rendered content.
   **/
  // the initial value of the 'isEditing' property
  isEditing: false,

  actions: {
    editTodo: function () {
      this.set('isEditing', true);
    },
    acceptChanges: function () {
      if (this && this.get('isEditing')) { // fixbug :: https://github.com/emberjs/ember.js/issues/3741 - https://github.com/emberjs/ember.js/issues/3741#issuecomment-28518220
        this.set('isEditing', false);

        if (Ember.isEmpty(this.get('model.title'))) {
          console.log("A borrar");
          this.send('removeTodo');
        } else {
          this.get('model').save();
        }
      }
    },
    /**
     * Because the todo is no longer part of the collection of all todos, its <li> element in the page will be automatically
     * removed for us. If the deleted todo was incomplete, the count of remaining todos will be decreased by one and the display
     * of this number will be automatically re-rendered. If the new count results in an inflection change between "item" and "items"
     * this area of the page will be automatically re-rendered.
     **/
    removeTodo: function () {
      var todo = this.get('model');
      console.log("Borrando");
      todo.deleteRecord();
      todo.save();
      console.log("Borrado");
    }
  },

  /**
   * Ember will call the computed property for both setters and getters, so if you want to use a computed property as
   * a setter, you'll need to check the number of arguments to determine whether it is being called as a getter or a setter.
   **/
  isCompleted: function(key, value){
    var model = this.get('model');

    if (value === undefined) {
      // property being used as a getter
      return model.get('isCompleted');
    } else {
      // property being used as a setter
      model.set('isCompleted', value);
      model.save();
      return value;
    }
  }.property('model.isCompleted'),
  /**
   * http://stackoverflow.com/questions/16983035/why-propertysomeproperty-used-in-computed-properties-in-ember-js
   *
   * The Computed Property .property() is cached by default. ie., the value is not computed everytime when you call the
   * property... To compute the value of the computed property again when any of its dependent key changes, we need to specify
   * its dependencies...
   * However we can turn off the cacheable option by using .property().volatile()
   *
   * When your content is an array, ArrayController plays the role... Assume you have an array of objects as [{task: 'xxx',
   * isCompleted: false},{task:'yyy', isCompleted: false},{task:'zzz', isCompleted: true}] whose 'isCompleted' property changes,
   * Now if you need a property with array of completed tasks, you can have it as a computed property with its dependent keys as
   * the content array... This will update your property when any of the array content Changes...
   **/

  /**
   * http://emberjs.com/api/classes/Ember.ComputedProperty.html#method_property
   *
   * (path) Ember.ComputedProperty
   * Defined in packages/ember-metal/lib/computed.js:258
   *
   * Sets the dependent keys on this computed property. Pass any number of arguments containing key paths that this computed
   * property depends on.
   *
   * MyApp.president = Ember.Object.create({
   *   fullName: Ember.computed(function() {
   *      return this.get('firstName') + ' ' + this.get('lastName');
   *      // Tell Ember that this computed property depends on firstName 
   *      // and lastName
   *  }).property('firstName', 'lastName')
   * });
   *
   *
   * Parameters:
   * path String
   *    zero or more property paths
   *
   * Returns:
   * Ember.ComputedProperty
   *    this
   *
   **/
});